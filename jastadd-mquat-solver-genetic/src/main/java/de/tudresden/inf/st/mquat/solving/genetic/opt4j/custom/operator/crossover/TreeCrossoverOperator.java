package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.crossover;

import com.google.inject.Inject;
import de.tudresden.inf.st.mquat.jastadd.model.Implementation;
import de.tudresden.inf.st.mquat.jastadd.model.Instance;
import de.tudresden.inf.st.mquat.jastadd.model.Request;
import de.tudresden.inf.st.mquat.jastadd.model.Resource;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotype;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotypeNode;
import org.opt4j.core.common.random.Rand;
import org.opt4j.operators.Apply;
import org.opt4j.operators.crossover.Crossover;
import org.opt4j.operators.crossover.Pair;

import java.util.Random;

@Apply(TreeGenotype.class)
public class TreeCrossoverOperator implements Crossover<TreeGenotype<Request, Instance, Resource, Implementation>> {

  private final Random random;
  private static final double Min = 0.0d;
  private static final double Max = 1.0d;
  private static final double probability = 0.5;

  @Inject
  public TreeCrossoverOperator(Rand random) {
    this.random = random;
  }

  @Override
  public Pair<TreeGenotype<Request, Instance, Resource, Implementation>> crossover(
      TreeGenotype<Request, Instance, Resource, Implementation> oldGenotype1,
      TreeGenotype<Request, Instance, Resource, Implementation> oldGenotype2) {

    TreeGenotype<Request, Instance, Resource, Implementation> newGenotype1 = oldGenotype1.newInstance();
    TreeGenotype<Request, Instance, Resource, Implementation> newGenotype2 = oldGenotype2.newInstance();

    for (Request request : oldGenotype1.getRequestMap().keySet()) {

      TreeGenotypeNode childOfOldGenotype1 = oldGenotype1.getRequestMap().get(request);
      TreeGenotypeNode childOfOldGenotype2 = oldGenotype2.getRequestMap().get(request);

      Pair<TreeGenotypeNode<Request, Instance, Resource, Implementation>> crossedPair = nodeStructureCrossover(childOfOldGenotype1, newGenotype1, childOfOldGenotype2, newGenotype2);

      newGenotype1.getRequestMap().put(request, crossedPair.getFirst());
      newGenotype2.getRequestMap().put(request, crossedPair.getSecond());
    }

    return new Pair<>(newGenotype1, newGenotype2);
  }

  private Pair<TreeGenotypeNode<Request, Instance, Resource, Implementation>> nodeStructureCrossover(
      TreeGenotypeNode<Request, Instance, Resource, Implementation> oldNode1,
      TreeGenotype<Request, Instance, Resource, Implementation> newContainer1,
      TreeGenotypeNode<Request, Instance, Resource, Implementation> oldNode2,
      TreeGenotype<Request, Instance, Resource, Implementation> newContainer2) {

    TreeGenotypeNode<Request, Instance, Resource, Implementation> newNode1 = new TreeGenotypeNode<>(newContainer1);
    TreeGenotypeNode<Request, Instance, Resource, Implementation> newNode2 = new TreeGenotypeNode<>(newContainer2);

    // if both nodes have the same impl and resource, recurse
    if (oldNode1.getImpl() == oldNode2.getImpl() && oldNode1.getResource() == oldNode2.getResource()) {

      // transfer impl and resource from old to new
      newNode1.setImpl(oldNode1.getImpl());
      newNode1.setResource(oldNode1.getResource());
      newNode2.setImpl(oldNode2.getImpl());
      newNode2.setResource(oldNode2.getResource());

      // call the crossover on all child substructures
      for (Instance instance : oldNode1.getSubStructure().keySet()) {
        TreeGenotypeNode childOfOldNode1 = oldNode1.getSubStructure().get(instance);
        TreeGenotypeNode childOfOldNode2 = oldNode2.getSubStructure().get(instance);
        Pair<TreeGenotypeNode<Request, Instance, Resource, Implementation>> crossedPair = nodeStructureCrossover(childOfOldNode1, newContainer1, childOfOldNode2, newContainer2);

        newNode1.getSubStructure().put(instance, crossedPair.getFirst());
        newNode2.getSubStructure().put(instance, crossedPair.getSecond());
      }
    } else {
      // TODO figure out what to swap

      double randomValue = Min + (Max - Min) * this.random.nextDouble();

      boolean swapImpl = this.random.nextBoolean();
      boolean swapResource = this.random.nextBoolean();

      if (swapImpl) {
        newNode1.setImpl(oldNode2.getImpl());
        newNode2.setImpl(oldNode1.getImpl());

        // add swapped substructures
        for (Instance instance : oldNode1.getSubStructure().keySet()) {
          newNode2.getSubStructure().put(instance, oldNode1.getSubStructure().get(instance).deepCopy());
        }
        for (Instance instance : oldNode2.getSubStructure().keySet()) {
          newNode1.getSubStructure().put(instance, oldNode2.getSubStructure().get(instance).deepCopy());
        }
      } else {
        newNode1.setImpl(oldNode1.getImpl());
        newNode2.setImpl(oldNode2.getImpl());

        // add swapped substructures
        for (Instance instance : oldNode1.getSubStructure().keySet()) {
          newNode1.getSubStructure().put(instance, oldNode1.getSubStructure().get(instance).deepCopy());
        }
        for (Instance instance : oldNode2.getSubStructure().keySet()) {
          newNode2.getSubStructure().put(instance, oldNode2.getSubStructure().get(instance).deepCopy());
        }
      }

      if (swapResource) {
        newNode1.setResource(oldNode2.getResource());
        newNode2.setResource(oldNode1.getResource());
      } else {
        newNode1.setResource(oldNode1.getResource());
        newNode2.setResource(oldNode2.getResource());
      }
    }

    return new Pair<>(newNode1, newNode2);
  }

}