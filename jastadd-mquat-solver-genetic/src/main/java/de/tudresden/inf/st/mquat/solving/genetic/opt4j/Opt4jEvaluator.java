package de.tudresden.inf.st.mquat.solving.genetic.opt4j;

import de.tudresden.inf.st.mquat.jastadd.model.Solution;
import org.opt4j.core.Objective.Sign;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;

public class Opt4jEvaluator implements Evaluator<Solution> {

  @Override
  public Objectives evaluate(Solution phenotype) {
    Objectives objectives = new Objectives();

    int validity = phenotype.evaluateValidity();
    int softwareValidity = phenotype.evaluateSoftwareValidity();

    objectives.add("Validity Errors", Sign.MIN, phenotype.evaluateValidity());
    objectives.add("Software Validity Errors", Sign.MIN, phenotype.evaluateSoftwareValidity());

    double objective = phenotype.computeObjective();

    objective += validity * 0.5 * objective;
    objective += softwareValidity * 0.5 * objective;

    objectives.add("Energy", Sign.MIN, objective);

    return objectives;
  }

}
