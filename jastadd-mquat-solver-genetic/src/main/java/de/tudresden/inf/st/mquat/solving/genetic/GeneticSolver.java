package de.tudresden.inf.st.mquat.solving.genetic;

import de.tudresden.inf.st.mquat.generator.ScenarioDescription;
import de.tudresden.inf.st.mquat.jastadd.model.Root;
import de.tudresden.inf.st.mquat.jastadd.model.Solution;
import de.tudresden.inf.st.mquat.solving.BenchmarkableSolver;
import de.tudresden.inf.st.mquat.solving.Solver;
import de.tudresden.inf.st.mquat.solving.SolvingException;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.Opt4jModule;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.copy.TreeCopyOperatorModule;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.crossover.TreeCrossoverOperatorModule;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.mutate.TreeMutateOperatorModule;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.operators.diversity.TreeDiversityOperator;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.operators.diversity.TreeDiversityOperatorModule;
import de.tudresden.inf.st.mquat.utils.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.start.Opt4JTask;
import org.opt4j.optimizers.ea.EvolutionaryAlgorithmModule;
import org.opt4j.optimizers.ea.Nsga2Module;
import org.opt4j.viewer.ViewerModule;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GeneticSolver implements BenchmarkableSolver {

  private static final Logger logger = LogManager.getLogger(GeneticSolver.class);

  private Solution lastSolution;
  private long lastSolvingTime;

  private int solutionCounter;

  private StopWatch stopWatch;

  private long maxSolvingTime;
  private boolean timedOut;

  public GeneticSolver() {
    this(Long.MAX_VALUE);
  }

  public GeneticSolver(long maxSolvingTime) {
    this.maxSolvingTime = maxSolvingTime;
    reset();
  }

  @Override
  public Solution solve(Root model) throws SolvingException {
    reset();

    stopWatch = StopWatch.start();

    List<Solution> solutions = new ArrayList<>();

    if (model.getNumRequest() == 0) {
      return Solution.emptySolutionOf(model);
    }


    Opt4jModule.setModel(model);

    EvolutionaryAlgorithmModule ea = new EvolutionaryAlgorithmModule();
    ea.setGenerations(20000);

    // set population size
    ea.setAlpha(100);

    Opt4jModule mquatModule = new Opt4jModule();

    TreeCrossoverOperatorModule crossover = new TreeCrossoverOperatorModule();
    TreeMutateOperatorModule mutate = new TreeMutateOperatorModule();
    TreeCopyOperatorModule copy = new TreeCopyOperatorModule();

//    ViewerModule viewer = new ViewerModule();
//    viewer.setCloseOnStop(false);

    Opt4JTask task = new Opt4JTask(false);
    task.init(ea, mquatModule, crossover, mutate, copy);
//    task.init(diversity, ea, nsga2, mquatModule, viewer, crossover, mutate, copy);
    try {
      task.execute();
      Archive archive = task.getInstance(Archive.class);
      for (Individual individual : archive) {
        // obtain the phenotype and objective, etc. of each individual
        Solution solution = (Solution) individual.getPhenotype();
        if (solution.isValid()) {
          if (solutions.isEmpty() || solution.computeObjective() < solutions.get(solutions.size() - 1).computeObjective()) {
            Solution clone = solution.deepCopy();
            solutions.add(clone);
            logger.info("found a better solution with an objective of {}.", solution.computeObjective());
          }
        } else {
          logger.warn("Found an invalid solution with " + solution.evaluateValidity() + " errors.");
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      task.close();
    }

    if (solutions.size() > 0) {
      lastSolution = solutions.get(solutions.size() - 1);
    } else {
      lastSolution = Solution.emptySolutionOf(model);
      logger.warn("Found no solution!");
    }

    lastSolvingTime = stopWatch.time(TimeUnit.MILLISECONDS);

    return lastSolution;
  }

  private void reset() {
    this.lastSolution = null;
    this.solutionCounter = 0;
    this.lastSolvingTime = 0;
    this.timedOut = false;
  }

  @Override
  public String getName() {
    return "genetic";
  }

  @Override
  public long getLastSolvingTime() {
    return lastSolvingTime;
  }

  @Override
  public Solver setTimeout(long timeoutValue, TimeUnit timeoutUnit) {
    this.maxSolvingTime = timeoutUnit.toMillis(timeoutValue);
    return this;
  }

  @Override
  public boolean hadTimeout() {
    return this.timedOut;
  }
}
