package de.tudresden.inf.st.mquat.solving.genetic.opt4j.operators.diversity;

import org.opt4j.operators.diversity.DiversityModule;

public class TreeDiversityOperatorModule extends DiversityModule {
  @Override
  protected void config() {
    addOperator(TreeDiversityOperator.class);
  }
}
