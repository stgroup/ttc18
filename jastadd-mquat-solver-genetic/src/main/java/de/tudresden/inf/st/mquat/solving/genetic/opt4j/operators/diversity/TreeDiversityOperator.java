package de.tudresden.inf.st.mquat.solving.genetic.opt4j.operators.diversity;

import de.tudresden.inf.st.mquat.jastadd.model.Implementation;
import de.tudresden.inf.st.mquat.jastadd.model.Instance;
import de.tudresden.inf.st.mquat.jastadd.model.Request;
import de.tudresden.inf.st.mquat.jastadd.model.Resource;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotype;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opt4j.operators.diversity.Diversity;

public class TreeDiversityOperator implements Diversity<TreeGenotype<Request, Instance, Resource, Implementation>> {

  private static final Logger logger = LogManager.getLogger(TreeDiversityOperator.class);

  @Override
  public double diversity(TreeGenotype<Request, Instance, Resource, Implementation> a, TreeGenotype<Request, Instance, Resource, Implementation> b) {


    logger.debug("DIVERSITY");
    System.out.println("DIVERSITY");

    return 1;

  }
}
