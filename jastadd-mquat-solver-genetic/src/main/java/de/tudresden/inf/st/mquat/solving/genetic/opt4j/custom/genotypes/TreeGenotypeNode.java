package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes;

import java.util.*;

public class TreeGenotypeNode<RqT, InT, ReT, ImT> {
  private ImT impl;
  private ReT res;
  private Map<InT, TreeGenotypeNode<RqT, InT, ReT, ImT>> subNodes;
  private TreeGenotype<RqT, InT, ReT, ImT> containingTreeGenotype;

  public TreeGenotypeNode(TreeGenotype genotype) {
    this.containingTreeGenotype = genotype;
    this.subNodes = new HashMap<>();
  }

  public ImT getImpl() {
    return impl;
  }

  public void setImpl(ImT i) {
    this.impl = i;
  }

  public TreeGenotype getContainingGenotype() {
    return containingTreeGenotype;
  }

  public void setContainingGenotype(TreeGenotype genotype) {
    this.containingTreeGenotype = genotype;
    for (TreeGenotypeNode subnode : subNodes.values()) {
      subnode.setContainingGenotype(genotype);
    }
  }

  public ReT getResource() {
    return res;
  }

  public void setResource(ReT r) {
    this.res = r;
  }

  public Map<InT, TreeGenotypeNode<RqT, InT, ReT, ImT>> getSubStructure() {
    return subNodes;
  }

  @Override
  public String toString() {
    return "[i=" + impl + ", r=" + res + ", c=" + subNodes + "]";
  }

  public TreeGenotypeNode<RqT, InT, ReT, ImT> deepCopy() {

    TreeGenotypeNode<RqT, InT, ReT, ImT> copy = new TreeGenotypeNode<>(getContainingGenotype());

    copy.setResource(this.getResource());
    copy.setImpl(this.getImpl());

    for (InT instance : this.getSubStructure().keySet()) {
      copy.getSubStructure().put(instance, this.getSubStructure().get(instance).deepCopy());
    }
    return copy;
  }

  public Collection<ReT> containedResources() {

    List<ReT> resources = new ArrayList<>();
    resources.add(this.getResource());
    for (TreeGenotypeNode subNode : subNodes.values()) {
      resources.addAll(subNode.containedResources());
    }

    return resources;
  }
}
