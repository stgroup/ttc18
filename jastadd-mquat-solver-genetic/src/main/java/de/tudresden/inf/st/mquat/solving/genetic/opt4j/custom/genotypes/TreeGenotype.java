package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes;

import org.opt4j.core.Genotype;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeGenotype<RqT, InT, ReT, ImT> implements Genotype {

  private List<ReT> resources;
  private Map<RqT, TreeGenotypeNode<RqT, InT, ReT, ImT>> requestMap = new HashMap<>();

  public TreeGenotype(List<ReT> resources) {
    this.resources = resources;
  }

  public List<ReT> getResources() {
    return resources;
  }

  @Override
  public int size() {
    // TODO recurse to find out size
    return 0;
  }

  @SuppressWarnings("unchecked")
  @Override
  public TreeGenotype<RqT, InT, ReT, ImT> newInstance() {
    try {
      Constructor<? extends TreeGenotype> cstr = this.getClass().getConstructor(List.class);
      return (TreeGenotype<RqT, InT, ReT, ImT>) cstr.newInstance(getResources());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public Map<RqT, TreeGenotypeNode<RqT, InT, ReT, ImT>> getRequestMap() {
    return requestMap;
  }

  @Override
  public String toString() {
    return "[i=" + requestMap + "]";
  }

  public TreeGenotype<RqT, InT, ReT, ImT> deepCopy() {
    TreeGenotype<RqT, InT, ReT, ImT> copy = this.newInstance();

    for (RqT request : requestMap.keySet()) {
      copy.getRequestMap().put(request, this.getRequestMap().get(request).deepCopy());
    }

    return copy;
  }

}
