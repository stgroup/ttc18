package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.copy;

import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotype;
import org.opt4j.operators.Apply;
import org.opt4j.operators.copy.Copy;

@Apply(TreeGenotype.class)
public class TreeCopyOperator implements Copy<TreeGenotype> {

  @Override
  public TreeGenotype copy(TreeGenotype genotype) {

    return genotype.deepCopy();
  }

}
