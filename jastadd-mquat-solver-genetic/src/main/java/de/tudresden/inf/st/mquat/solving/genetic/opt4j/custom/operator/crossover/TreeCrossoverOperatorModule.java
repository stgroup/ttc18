package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.crossover;

import org.opt4j.operators.crossover.CrossoverModule;

public class TreeCrossoverOperatorModule extends CrossoverModule {

  @Override
  protected void config() {
    // TODO Auto-generated method stub

    addOperator(TreeCrossoverOperator.class);
  }

//	@Override
//	protected void configure() {
//		// TODO Auto-generated method stub
//		bind(TreeOperator.class).to(TreeOperator.class);
//	}
//

}