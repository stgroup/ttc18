package de.tudresden.inf.st.mquat.solving.genetic.opt4j;

import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotype;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotypeNode;
import org.opt4j.core.problem.Decoder;

import java.util.Map;

public class Opt4jDecoder implements Decoder<TreeGenotype<Request, Instance, Resource, Implementation>, Solution> {


  private static void populateResourceMapping(ResourceMapping mapping, ResourceRequirement requirement,
                                              Resource resource) {
    for (ResourceRequirement subRequirement : requirement.getResourceRequirementList()) {
      int fittingResourceCount = 0;
      for (int currentInstance = 0; currentInstance < subRequirement.getNumInstance(); currentInstance++) {
        for (int currentResource = 0; currentResource < resource.getNumSubResource(); currentResource++) {
          Resource subResource = resource.getSubResource(currentResource);
          if (subResource.getType().getRef() == subRequirement.getResourceTypeRef().getRef()) {
            if (currentInstance == fittingResourceCount) {
              ResourceMapping newMapping = new ResourceMapping();
              Instance instance = subRequirement.getInstance(currentInstance);
              newMapping.setInstance(instance);
              newMapping.setResource(subResource);
              mapping.addResourceMapping(newMapping);
              populateResourceMapping(newMapping, subRequirement, subResource);
              fittingResourceCount++;
            }
            currentInstance++;
          }
        }
      }
    }
  }

  @Override
  public Solution decode(TreeGenotype<Request, Instance, Resource, Implementation> genotype) {
    Solution phenotype = new Solution();
    phenotype.setModel(Opt4jModule.getModel());
    for (Map.Entry<Request, TreeGenotypeNode<Request, Instance, Resource, Implementation>> e : genotype.getRequestMap()
        .entrySet()) {

      phenotype.addAssignment(decodeNode(true, e.getKey(), e.getValue()));
    }

    return phenotype;
  }

  private Assignment decodeNode(boolean isTopLevel, Request request, TreeGenotypeNode<Request, Instance, Resource, Implementation> node) {

    Assignment assignment = new Assignment();

    assignment.setTopLevel(isTopLevel);
    assignment.setRequest(request);
    assignment.setImplementation(node.getImpl());

    // fix the resource mapping
    // we assume that exactly one resource is required
    Instance resourceInstance = node.getImpl().getResourceRequirement().getInstance(0);
    ResourceMapping rm = new ResourceMapping();
    rm.setInstance(resourceInstance);
    rm.setResource(node.getResource());
    populateResourceMapping(rm, node.getImpl().getResourceRequirement(), node.getResource());
    assignment.setResourceMapping(rm);

    for (Map.Entry<Instance, TreeGenotypeNode<Request, Instance, Resource, Implementation>> e : node.getSubStructure()
        .entrySet()) {

      assignment.addComponentMapping(new ComponentMapping(e.getKey(), decodeNode(false, request, e.getValue())));
    }
    return assignment;
  }
}
