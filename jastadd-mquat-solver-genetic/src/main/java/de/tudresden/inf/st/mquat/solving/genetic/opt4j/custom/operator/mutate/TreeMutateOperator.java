package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.mutate;

import com.google.inject.Inject;
import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.Opt4jCreator;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotype;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotypeNode;
import org.opt4j.core.common.random.Rand;
import org.opt4j.operators.Apply;
import org.opt4j.operators.mutate.Mutate;

import java.util.List;
import java.util.Random;

@Apply(TreeGenotype.class)
public class TreeMutateOperator implements Mutate<TreeGenotype<Request, Instance, Resource, Implementation>> {

  private final Random random;
  private static final double Min = 0.0d;
  private static final double Max = 1.0d;

  @Inject
  public TreeMutateOperator(Rand random) {
    this.random = random;
  }

  @Override
  public void mutate(TreeGenotype<Request, Instance, Resource, Implementation> genotype, double p) {

    p = 0.9;

    for (Request e : genotype.getRequestMap()
        .keySet()) {

      TreeGenotypeNode<Request, Instance, Resource, Implementation> node = genotype.getRequestMap().get(e);

      double randomValue = Min + (Max - Min) * this.random.nextDouble();

      if (randomValue < p) {
        genotype.getRequestMap().put(e, mutateNode(e, node, p));
      }

    }
  }

  private TreeGenotypeNode<Request, Instance, Resource, Implementation> mutateNode(Request request,
      TreeGenotypeNode<Request, Instance, Resource, Implementation> node, double p) {



    double randomValue = Min + (Max - Min) * random.nextDouble();

    if (randomValue < p/3.0) {
      List<Resource> unassignedResources = node.getContainingGenotype().getResources();
      if (random.nextDouble() > 0.7) {
        // mutate the resources
        unassignedResources.add(node.getResource());
        // get a random unassigned resource
        int resourceIndex = random.nextInt(unassignedResources.size());
        node.setResource(unassignedResources.get(resourceIndex));
        unassignedResources.remove(resourceIndex);
      }
      if (random.nextDouble() > 0.4) {
        // mutate the implementation
        Component comp = node.getImpl().containingComponent();
        Implementation newImp = comp.getImplementation(random.nextInt(comp.getNumImplementation()));

        // clear substructure requirements

        // add newly freed resources
        for (TreeGenotypeNode subNode : node.getSubStructure().values()) {
          unassignedResources.addAll(subNode.containedResources());
        }
        node.getSubStructure().clear();

        node.setImpl(newImp);

        for (ComponentRequirement cr : newImp.getComponentRequirementList()) {
          for (Instance ci : cr.getInstanceList()) {
            // create node
            Assignment newAssignment = newImp.root().createRandomAssignment(false, request, cr.getComponentRef().getRef(), unassignedResources, new Random());
            node.getSubStructure().put(ci, Opt4jCreator.createNode(newAssignment, node.getContainingGenotype()));
          }
        }
      }



    } else {
      // mutate the children
      for (Instance instance : node.getSubStructure().keySet()) {
        node.getSubStructure().put(instance, mutateNode(request, node.getSubStructure().get(instance), p));
      }
    }

    return node;
  }

}
