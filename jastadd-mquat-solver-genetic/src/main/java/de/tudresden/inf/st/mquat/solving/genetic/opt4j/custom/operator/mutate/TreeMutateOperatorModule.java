package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.mutate;

import org.opt4j.operators.mutate.MutateModule;

public class TreeMutateOperatorModule extends MutateModule {

	@Override
	protected void config() {
		addOperator(TreeMutateOperator.class);
	}

}
