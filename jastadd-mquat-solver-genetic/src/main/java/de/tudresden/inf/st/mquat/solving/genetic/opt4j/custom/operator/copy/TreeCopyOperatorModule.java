package de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.operator.copy;

import org.opt4j.operators.copy.CopyModule;

public class TreeCopyOperatorModule extends CopyModule {

  @Override
  protected void config() {
    addOperator(TreeCopyOperator.class);
  }

}
