package de.tudresden.inf.st.mquat.solving.genetic.opt4j;

import de.tudresden.inf.st.mquat.jastadd.model.Root;
import org.opt4j.core.problem.ProblemModule;

public class Opt4jModule extends ProblemModule {

  private static Root model;

  public static Root getModel() {
    return model;
  }

  public static void setModel(Root model) {
    Opt4jModule.model = model;
  }

  @Override
  protected void configure() {
    bindProblem(Opt4jCreator.class, Opt4jDecoder.class, Opt4jEvaluator.class);
  }

  @Override
  protected void config() {
  }

}
