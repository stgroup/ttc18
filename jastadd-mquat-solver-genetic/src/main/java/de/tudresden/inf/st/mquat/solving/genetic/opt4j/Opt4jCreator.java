package de.tudresden.inf.st.mquat.solving.genetic.opt4j;

import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotype;
import de.tudresden.inf.st.mquat.solving.genetic.opt4j.custom.genotypes.TreeGenotypeNode;
import org.opt4j.core.problem.Creator;

import java.util.Random;

public class Opt4jCreator implements Creator<TreeGenotype> {

  public static TreeGenotypeNode<Request, Instance, Resource, Implementation> createNode(Assignment assignment, TreeGenotype<Request, Instance, Resource, Implementation> container) {

    TreeGenotypeNode<Request, Instance, Resource, Implementation> node = new TreeGenotypeNode<>(container);
    node.setImpl(assignment.getImplementation());
    node.setResource(assignment.getResource());
    for (ComponentMapping subAssignment : assignment.getComponentMappingList()) {
      node.getSubStructure().put(subAssignment.getInstance(), createNode(subAssignment.getAssignment(), container));
    }

    return node;
  }

  public TreeGenotype<Request, Instance, Resource, Implementation> create() {

    // TODO this method create a phenotype and translates it to a genotype. this is not nice!

    Root model = Opt4jModule.getModel();

    Solution phenotype = model.createRandomSolution(new Random());

    TreeGenotype<Request, Instance, Resource, Implementation> genotype = new TreeGenotype<>(model.getHardwareModel().allContainers());

    for (Assignment a : phenotype.getAssignmentList()) {
      if (a.getTopLevel() && a.getImplementation().getName().getName().length() > 19) {
        throw new RuntimeException();
      }
      genotype.getRequestMap().put(a.getRequest(), createNode(a, genotype));
    }

    return genotype;
  }
}
