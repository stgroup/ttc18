package de.tudresden.inf.st.mquat.solving;

import de.tudresden.inf.st.mquat.solving.genetic.GeneticSolver;

public class GeneticHandwrittenTest extends HandwrittenTestSuite {

  @Override
  protected Solver getSolver() {
    return new GeneticSolver(10000);
  }
}
