package de.tudresden.inf.st.mquat.solving;

import de.tudresden.inf.st.mquat.solving.random.RandomSolver;

public class RandomHandwrittenTest extends HandwrittenTestSuite {

  @Override
  protected Solver getSolver() {
    return new RandomSolver(5000, 0);
  }
}
