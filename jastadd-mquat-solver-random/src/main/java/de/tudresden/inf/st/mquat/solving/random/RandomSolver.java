package de.tudresden.inf.st.mquat.solving.random;

import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.solving.BenchmarkableSolver;
import de.tudresden.inf.st.mquat.solving.Solver;
import de.tudresden.inf.st.mquat.solving.SolverUtils;
import de.tudresden.inf.st.mquat.solving.SolvingException;
import de.tudresden.inf.st.mquat.utils.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RandomSolver implements BenchmarkableSolver {

  private static final Logger logger = LogManager.getLogger(RandomSolver.class);

  private Solution lastSolution;
  private long lastSolvingTime;

  private int solutionCounter;

  private StopWatch stopWatch;

  private long maxSolvingTime;
  private boolean timedOut;

  private final Random random;

  public RandomSolver(long maxSolvingTime, long seed) {
    this.maxSolvingTime = maxSolvingTime;
    this.random = new Random(seed);
    reset();
  }


  @Override
  public Solution solve(Root model) throws SolvingException {
    reset();
    if (model.getNumRequest() == 0) {
      return Solution.emptySolutionOf(model);
    }
    int numAssignments = 0;
    int numTotalSoftwareSolutions = 0;

    stopWatch = StopWatch.start();

    List<Solution> solutions = new ArrayList<>();

    do {

      numTotalSoftwareSolutions++;

      Solution solution = model.createRandomSolution(random);


      if (solution.isValid()) {
        solutionCounter++;
        if (solutions.isEmpty() || solution.computeObjective() < solutions.get(solutions.size() - 1).computeObjective()) {
          solutions.add(solution);
          logger.info("found a better solution with an objective of {}.", solution.computeObjective());
        }
      }

      if (stopWatch.time(TimeUnit.MILLISECONDS) > maxSolvingTime) {
        this.timedOut = true;
        logger.warn("Timeout! Solving terminated!");
        break;
      }

    } while (true);

    logger.info("Number of iterated solutions: {}", numTotalSoftwareSolutions);
    logger.info("Number of correct solutions: {}", solutionCounter);

    if (solutions.size() > 0) {
      lastSolution = solutions.get(solutions.size() - 1);
    } else {
      lastSolution = Solution.emptySolutionOf(model);
      logger.warn("Found no solution!");
    }

    lastSolvingTime = stopWatch.time(TimeUnit.MILLISECONDS);

    return lastSolution;
  }

  private void reset() {
    this.lastSolution = null;
    this.solutionCounter = 0;
    this.lastSolvingTime = 0;
    this.timedOut = false;
  }

  @Override
  public String getName() {
    return "random";
  }

  @Override
  public long getLastSolvingTime() {
    return lastSolvingTime;
  }

  @Override
  public Solver setTimeout(long timeoutValue, TimeUnit timeoutUnit) {
    this.maxSolvingTime = timeoutUnit.toMillis(timeoutValue);
    return this;
  }

  @Override
  public boolean hadTimeout() {
    return this.timedOut;
  }
}
