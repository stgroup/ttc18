package de.tudresden.inf.st.mquat;

import beaver.Parser;
import de.tudresden.inf.st.mquat.generator.ScenarioDescription;
import de.tudresden.inf.st.mquat.generator.ScenarioGenerator;
import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.jastadd.parser.MquatParser;
import de.tudresden.inf.st.mquat.jastadd.scanner.MquatScanner;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Parser and pretty-printer test.
 *
 * @author rschoene - Initial contribution
 */
public class PPPTest {

  private static final ScenarioDescription SCENARIO_DESCRIPTION = new ScenarioDescription(2, 2, 0, 0, 0, 2, 2, 1.5, 2, 2, 0);
  private static final Path ERROR_PATH = Paths.get(".");

  private static Tuple<Root, Solution> generateNewModel() {
    ScenarioGenerator generator = new ScenarioGenerator(SCENARIO_DESCRIPTION);
    Root model = generator.generate();
    Solution solution = generator.getInitialSolution();
    return Tuple.of(model, solution);
  }

  @Test
  public void roundtripTest() throws IOException, Parser.Exception {
    MquatWriteSettings settings = new MquatWriteSettings(" ");
    // 1. Generate a model, and use initial solution
    Tuple<Root, Solution> modelAndSolution = generateNewModel();
    // 2. Print it as a string (model and solution), no need to use files
    final String firstModel = modelAndSolution.getFirstElement().print(settings).toString();
    final String firstSolution = modelAndSolution.getSecondElement().print(settings).toString();
    // 3. Read/Parse both strings again
    Tuple<Root, Solution> modelAndSolution2 = parse(firstModel, firstSolution);
    // 4. Print them again
    final String secondModel = modelAndSolution2.getFirstElement().print(settings).toString();
    final String secondSolution = modelAndSolution2.getSecondElement().print(settings).toString();
    // 5. Check if both are syntactically equal
    assertEquals("Models are not equal", firstModel, secondModel);
    assertEquals("Solutions are not equal", firstSolution, secondSolution);
  }

  private Tuple<Root, Solution> parse(String modelContent, String solutionContent) throws IOException, Parser.Exception {
    MquatParser parser = new MquatParser();

    StringReader reader = new StringReader(modelContent);
    MquatScanner scanner = new MquatScanner(reader);
    Root model = (Root) parser.parse(scanner);
    parser.resolveReferences();

    reader = new StringReader(solutionContent);
    scanner = new MquatScanner(reader);
    Solution solution = (Solution) parser.parse(scanner, MquatParser.AltGoals.solution);
    parser.resolveSolutionReferencesWith(model);

    return Tuple.of(model, solution);
  }
}
