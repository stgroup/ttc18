package de.tudresden.inf.st.mquat.solving.ilp;

import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.solving.BenchmarkableSolver;
import de.tudresden.inf.st.mquat.solving.SolverUtils;
import de.tudresden.inf.st.mquat.solving.SolvingException;
import de.tudresden.inf.st.mquat.utils.StaticSettings;
import de.tudresden.inf.st.mquat.utils.StopWatch;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class AbstractILPSolver implements BenchmarkableSolver {

  protected transient final Logger logger;
  protected long lastGeneration;
  private long lastSolving;
  private long lastSolutionCreation;
  protected transient long timeoutValue;
  protected transient TimeUnit timeoutUnit;
  protected boolean timedOut;

  /**
   * Create a new, abstract solver with default settings.
   * Defaults are:
   * <ul>
   *   <li>1 minute timeout</li>
   * </ul>
   * @param logger the logger to use
   * @see ILPDirectSolver#setTimeout(long, TimeUnit)
   */
  public AbstractILPSolver(Logger logger) {
    this.logger = logger;
    setTimeout(1, TimeUnit.MINUTES);
    reset();
  }

  protected void cleanup() {
    // empty default implementation
  }

  protected long getLastSolving() {
    return lastSolving;
  }

  /**
   * Reset times and the objective (i.e., all member fields beginning with "last").
   */
  protected void reset() {
    this.lastGeneration = 0;
    this.lastSolving = 0;
    this.lastSolutionCreation = 0;
    this.timedOut = false;
  }

  @Override
  public synchronized Solution solve(Root model) throws SolvingException {
    StopWatch watch = StopWatch.start();

    reset();
    if (model.getNumRequest() == 0) {
      return Solution.emptySolutionOf(model);
    }

    // generate the ILP NTA and take the generation time
    final ILP ilp = model.getILP();
    lastGeneration = watch.time(TimeUnit.MILLISECONDS);
    logger.debug("ILP-Generation took {}ms.", lastGeneration);
    if (ilp.hasTimeout()) {
      logger.error("ILP-Generation exceeded timeout, message: '{}'", ilp.timeoutReason());
      this.lastSolving = watch.time(TimeUnit.MILLISECONDS) - this.lastGeneration;
      return Solution.emptySolutionOf(model);
    }

    long nanosRemaining = this.timeoutUnit.toNanos(this.timeoutValue) - watch.time(TimeUnit.NANOSECONDS);
    if (nanosRemaining < 0) {
      logger.error("ILP-Generation did not report a timeout, but it actually did.");
      return Solution.emptySolutionOf(model);
    }

    // provide data structure for ilp-encoded solution
    List<IlpVariable> variablesSetToOne = new ArrayList<>();

    // call to abstract method
    try {
      solve0(model, ilp, watch, variablesSetToOne);
      this.lastSolving = watch.time(TimeUnit.MILLISECONDS) - this.lastGeneration;
      // translate ilp-encoded solution to MQuAT solution
      return populateSolution(variablesSetToOne, new ILPSolution(model));
    } finally {
      // always clean up
      cleanup();
    }
  }

  /**
   * Solves the model. The method <code>model.getILP()</code> was already called and can be assumed to be cached.
   * @param model             the model to solve
   * @param watch             a stop watch to be passed to cleanup if necessary
   * @param variablesSetToOne the means of a solution, i.e., which variables are set to one
   * @return the objective value
   * @throws SolvingException if anything went wrong
   */
  protected abstract double solve0(Root model, ILP ilp, StopWatch watch, List<IlpVariable> variablesSetToOne) throws SolvingException;

  protected ILPSolution populateSolution(List<IlpVariable> variablesSetToOne, ILPSolution result) throws SolvingException {
    List<Assignment> listOfAssignments = new ArrayList<>();
    for (IlpVariable var : variablesSetToOne) {
      if (var.isMappingVariable()) {
        IlpMappingVariable mappingVar = var.asMappingVariable();
        Assignment assignment = new Assignment();
        assignment.setRequest(mappingVar.getRequest());
        assignment.setImplementation(mappingVar.getImpl());
        assignment.setResourceMapping(new ResourceMapping(assignment.getImplementation().getResourceRequirement().getInstance(0), mappingVar.getResource(), new de.tudresden.inf.st.mquat.jastadd.model.List<>()));
        listOfAssignments.add(assignment);
      }
    }
    lastSolutionCreation = SolverUtils.populateSolution(listOfAssignments, result, logger);
    return result;
  }

  public AbstractILPSolver setTimeout(long timeoutValue, TimeUnit timeoutUnit) {
    this.timeoutUnit = timeoutUnit;
    this.timeoutValue = timeoutValue;
    StaticSettings.put(Root.ILP_TIMEOUT_VALUE, timeoutValue);
    StaticSettings.put(Root.ILP_TIMEOUT_UNIT, timeoutUnit);
    return this;
  }

  @Override
  public boolean doesGeneration() {
    return true;
  }

  @Override
  public long getLastGenerationTime() {
    return lastGeneration;
  }

  @Override
  public long getLastSolvingTime() {
    return lastSolving + lastSolutionCreation;
  }

  @Override
  public boolean hadTimeout() {
    return this.timedOut;
  }
}
