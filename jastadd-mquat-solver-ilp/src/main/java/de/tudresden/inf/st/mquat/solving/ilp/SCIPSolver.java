package de.tudresden.inf.st.mquat.solving.ilp;

import de.tudresden.inf.st.mquat.jastadd.model.ILP;
import de.tudresden.inf.st.mquat.jastadd.model.IlpVariable;
import de.tudresden.inf.st.mquat.solving.SolvingException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SCIPSolver extends ILPExternalSolver {

  /**
   * Create a new GLPK solver with default settings.
   * Default is:
   * <ul>
   * <li>1 minute timeout</li>
   * <li>delete temporary files on exit.</li>
   * </ul>
   *
   * @see SCIPSolver#setDeleteFilesOnExit(boolean)
   */
  public SCIPSolver() {
  }


  @Override
  protected String[] getCommand(Path lp, Path solution, long remainingTimeForSolvingInMillis) {
    String[] command = {"scip", "-c", "read " + lp.toAbsolutePath() + " set timing reading true set timing clocktype 2 set limit time " + remainingTimeForSolvingInMillis / 1000 + " optimize write solution " + solution.toAbsolutePath() + " quit"};
    return command;
  }

  @Override
  protected void readFromPlainTextSolution(ILP ilp, Path solution, ILPSolution result,
                                           List<IlpVariable> variablesSetToOne) throws SolvingException {
    try (Stream<String> lines = Files.lines(solution)) {
      for (String line : lines.collect(Collectors.toList())) {
        if (line.startsWith("objective value:")) {
          result.setObjective(Double.valueOf(line.substring(16).trim()));
          logger.debug("read objective {}", Double.valueOf(line.substring(16).trim()));
        } else if (line.startsWith("solution status:")) {
          logger.debug("we got a solution status {}", line.substring(16).trim());
        } else if (line.startsWith("no solution available")) {
          logger.debug("no solution was found!");
          return;
        } else {
          String[] tokens = line.split("\\s+");
          if (tokens.length == 3) {
            // tokens: name, value, objective

            if (Math.round(Double.parseDouble(tokens[1]) * 1000000000) == 1000000000) {
              logger.debug("found new variable {} with value {}", tokens[0], tokens[1]);
              IlpVariable variable = ilp.resolve(tokens[0]);
              if (variable == null) {
                throw new SolvingException("Could not find variable with name " + tokens[0]);
              }
              variablesSetToOne.add(variable);
            }
          }
        }
      }
    } catch (IOException e) {
      throw new SolvingException("Could not open solution file", e);
    } catch (NumberFormatException e) {
      throw new SolvingException("Could not parse solution file", e);
    }
  }

  @Override
  public String getName() {
    return "ilp-scip";
  }

}
