package de.tudresden.inf.st.mquat.solving.ilp;

import de.tudresden.inf.st.mquat.jastadd.model.ILP;
import de.tudresden.inf.st.mquat.jastadd.model.IlpVariable;
import de.tudresden.inf.st.mquat.solving.SolvingException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GLPKSolver extends ILPExternalSolver {

  /**
   * Create a new GLPK solver with default settings.
   * Default is:
   * <ul>
   *   <li>1 minute timeout</li>
   *   <li>delete temporary files on exit.</li>
   * </ul>
   * @see GLPKSolver#setDeleteFilesOnExit(boolean)
   */
  public GLPKSolver() {
  }

  @Override
  protected String[] getCommand(Path lp, Path solution, long remainingTimeForSolvingInMillis) {
    return new String[] {"glpsol", "--lp", lp.toAbsolutePath().toString(), "--cuts",
        "-o", String.valueOf(solution.toAbsolutePath())};
  }

  @Override
  public String getName() {
    return "ilp-glpk";
  }

  @Override
  protected void readFromPlainTextSolution(ILP ilp, Path solution, ILPSolution result,
                                                List<IlpVariable> variablesSetToOne) throws SolvingException {
    List<String> varNamesSetToOne = new ArrayList<>();
    String name = null;
    int phase = 1;
    try (Stream<String> lines = Files.lines(solution)) {
      for (String line : lines.collect(Collectors.toList())) {
        if (phase < 3) {
          if (line.startsWith("Objective")) {
            int equalsIndex = line.indexOf('=');
            int bracketIndex = line.lastIndexOf('(');
            result.setObjective(Double.valueOf(line.substring(equalsIndex + 1, bracketIndex).trim()));
          }
          if (line.startsWith("---")) {
            phase += 1;
          }
          continue;
        }
        line = line.trim();
        if (line.isEmpty()) {
          continue;
        }
        String[] tokens = line.split("\\s+");
        if (tokens.length == 6) {
          // tokens: index, name, star, activity, lb, rb
          if(Integer.valueOf(tokens[3]) == 1) {
            varNamesSetToOne.add(tokens[1]);
          }
          phase = 3;
        } else if (phase == 3) {
          if(line.startsWith("Integer")) {
            break;
          }
          // tokens: index, name
          name = tokens[1];
          phase = 4;
        } else if (phase == 4) {
          // tokens: star, activity, lb, rb
          if (name == null) {
            throw new SolvingException("Error in parsing solution. Name is null. Tokens: " + Arrays.toString(tokens));
          }
          if (Integer.valueOf(tokens[1]) == 1) {
            varNamesSetToOne.add(name);
            name = null;
          }
          phase = 3;
        }
      }
    } catch (IOException e) {
      throw new SolvingException("Could not open solution file", e);
    } catch (NumberFormatException | IndexOutOfBoundsException e) {
      throw new SolvingException("Could not parse solution file", e);
    }
    for (String varName : varNamesSetToOne) {
      IlpVariable variable = ilp.resolve(varName);
      if (variable == null) {
        throw new SolvingException("Could not find variable with name " + varName);
      }
      variablesSetToOne.add(variable);
    }
  }

}
