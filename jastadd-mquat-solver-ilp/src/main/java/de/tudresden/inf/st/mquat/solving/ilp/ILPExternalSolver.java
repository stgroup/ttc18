package de.tudresden.inf.st.mquat.solving.ilp;

import de.tudresden.inf.st.mquat.jastadd.model.*;
import de.tudresden.inf.st.mquat.solving.SolvingException;
import de.tudresden.inf.st.mquat.utils.StopWatch;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ILPExternalSolver extends AbstractILPSolver {

  private boolean deleteFilesOnExit;
  private Path lp, solutionReadable;

  /**
   * Create a new solver with default settings.
   * Default is:
   * <ul>
   *   <li>1 minute timeout</li>
   *   <li>delete temporary files on exit.</li>
   * </ul>
   * @see ILPExternalSolver#setDeleteFilesOnExit(boolean)
   */
  public ILPExternalSolver() {
    super(LogManager.getLogger(ILPExternalSolver.class));
    deleteFilesOnExit = true;
  }

  public ILPExternalSolver setDeleteFilesOnExit(boolean deleteFilesOnExit) {
    this.deleteFilesOnExit = deleteFilesOnExit;
    return this;
  }

  /**
   * Log stdout (always to logger.debug) and stderr (if existing to logger.warn)
   * @param process the given process to inspect
   */
  private void printFromProcess(Process process) {
    try (Scanner s = new Scanner(process.getInputStream())) {
      logger.debug(s.useDelimiter("\\A").hasNext() ? s.next() : "<no output>");
    }
    try (Scanner s = new Scanner(process.getErrorStream())) {
      if (s.useDelimiter("\\A").hasNext()) {
        logger.warn(s.next());
      }
    }
  }

  @Override
  protected void cleanup() {
    if (deleteFilesOnExit) {
      if (lp.toFile().exists() && !lp.toFile().delete()) {
        logger.warn("Could not delete ILP file {}", lp.toAbsolutePath());
      }
      if (solutionReadable.toFile().exists() && !solutionReadable.toFile().delete()) {
        logger.warn("Could not delete solution file {}", solutionReadable.toAbsolutePath());
      }
    }
  }

  protected double solve0(Root model, ILP ilp, StopWatch watch, List<IlpVariable> variablesSetToOne) throws SolvingException {

    long startOfWriteOutInMillis = watch.time(TimeUnit.MILLISECONDS);

    // Create temporary files
    try {
      lp = Files.createTempFile("ilp", ".lp");
//      solution = Files.createTempFile("solution", null);
      solutionReadable = Files.createTempFile("sol-read", ".sol");
    } catch (IOException e) { throw new SolvingException("Can not create lp or solution file", e); }
    if (!deleteFilesOnExit) {
      logger.info("Writing ILP to {}, solving now", lp.toAbsolutePath());
    }

    // write out lp file
    logger.debug("Starting ILP string construction.");
    IlpString output = ilp.printIlp();
    logger.debug("ILP string construction completed.");
    try (BufferedWriter writer = Files.newBufferedWriter(
        lp, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
      writer.write(output.toString());
    } catch (IOException e) {
      throw new SolvingException("Could not write to lp file", e);
    }
    this.lastGeneration = watch.time(TimeUnit.MILLISECONDS);
    long millisecondsNeededToWriteOut = watch.time(TimeUnit.MILLISECONDS) - startOfWriteOutInMillis;
    long remainingTimeInMillis = this.timeoutUnit.toMillis(this.timeoutValue) - lastGeneration;
    long remainingTimeForSolvingInMillis = Math.max(0, remainingTimeInMillis - 2*millisecondsNeededToWriteOut);
    // take twice the time to have buffer to write out solution afterwards


    // start GLPK to solve the lp file just written, writing out the solution
    Process process;
    String[] command = getCommand(lp, solutionReadable, remainingTimeForSolvingInMillis);
    logger.debug("Call: '{}'", String.join(" ", command));
    try {
      process = Runtime.getRuntime().exec(command,null, new File("."));
    } catch (IOException e) {
      throw new SolvingException("Problem calling solver. Is it installed?", e);
    }
    boolean finishedInTime;
    try {
      finishedInTime = process.waitFor(remainingTimeForSolvingInMillis, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      throw new SolvingException("Interrupted while waiting for result", e);
    }
    StopWatch writerWatch = StopWatch.start();
    if (!finishedInTime) {
      // solver already had a timeout, so wait some seconds longer to let it write a solution file
      this.timedOut = true;
      try {
        logger.debug("Solver had a timeout, waiting ten seconds to let it write the result.");
        process.waitFor(10, TimeUnit.SECONDS);
      } catch (InterruptedException ignored) { }
      logger.debug("Solver took {}ms to write the result.", writerWatch.time(TimeUnit.MILLISECONDS));
      // then destroy the process
      process.destroyForcibly();
      if (!solutionReadable.toAbsolutePath().toFile().exists()) {
        throw new SolvingException("Solving did not finish within " + timeoutValue + " " + timeoutUnit.toString()
            + ", file at " + solutionReadable.toAbsolutePath() + " was not written.");
      }
      // if there is a solution file, move on and check its content
    }
    printFromProcess(process);
    if (!solutionReadable.toFile().exists()) {
      throw new SolvingException("No solution file was created.");
    }
    logger.debug("Solution at {}", solutionReadable);

    // read the solution file
    ILPSolution result = new ILPSolution(model);

    logger.debug("created empty solution");

//    readFromPrintableSolution(ilp, solution, result, variablesSetToOne);
    readFromPlainTextSolution(ilp, solutionReadable, result, variablesSetToOne);
    return result.getObjective();
  }

  protected abstract void readFromPlainTextSolution(ILP ilp, Path solution, ILPSolution result,
                                                List<IlpVariable> variablesSetToOne) throws SolvingException;

  protected abstract String[] getCommand(Path lp, Path solution, long remainingTimeForSolvingInMillis);

}
