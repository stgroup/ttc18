package de.tudresden.inf.st.mquat.solving.ilp;

import de.tudresden.inf.st.mquat.jastadd.model.ILP;
import de.tudresden.inf.st.mquat.jastadd.model.IlpVariable;
import de.tudresden.inf.st.mquat.solving.SolvingException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GurobiSolver extends ILPExternalSolver {

  /**
   * Create a new GLPK solver with default settings.
   * Default is:
   * <ul>
   *   <li>1 minute timeout</li>
   *   <li>delete temporary files on exit.</li>
   * </ul>
   * @see GurobiSolver#setDeleteFilesOnExit(boolean)
   */
  public GurobiSolver() {

  }


  @Override
  protected String[] getCommand(Path lp, Path solution, long remainingTimeForSolvingInMillis) {
    String[] command = {"gurobi_cl", "ResultFile=" + solution.toAbsolutePath(), "TimeLimit=" + remainingTimeForSolvingInMillis/1000, String.valueOf(lp.toAbsolutePath())};
    return command;
  }

  @Override
  protected void readFromPlainTextSolution(ILP ilp, Path solution, ILPSolution result,
                                                List<IlpVariable> variablesSetToOne) throws SolvingException {

    logger.debug("reading solution from {}.", solution);

    try(BufferedReader br = new BufferedReader(new FileReader(solution.toFile()))) {
      for(String line; (line = br.readLine()) != null; ) {
        if (line.startsWith("#")) {
          // comment line

          if (line.startsWith("# Objective Value =")) {
            result.setObjective(Double.valueOf(line.substring(20).trim()));
            logger.debug("read objective {}", Double.valueOf(line.substring(20).trim()));
          }
        } else {
          if (line.endsWith(" 1")) {
            String varString = line.substring(0,line.length()-2);
            IlpVariable variable = ilp.resolve(varString);
            if (variable == null) {
              throw new SolvingException("Could not find variable with name " + varString);
            }
            variablesSetToOne.add(variable);
          }
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getName() {
    return "ilp-gurobi";
  }

}
