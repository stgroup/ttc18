package de.tudresden.inf.st.mquat.solving;

import de.tudresden.inf.st.mquat.solving.ilp.SCIPSolver;

public class SCIPHandwrittenTest extends HandwrittenTestSuite {
  @Override
  protected Solver getSolver() {
    // set to false for debugging
    return new SCIPSolver().setDeleteFilesOnExit(false);
  }
}
