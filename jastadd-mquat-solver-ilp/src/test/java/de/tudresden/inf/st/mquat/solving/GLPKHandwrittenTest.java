package de.tudresden.inf.st.mquat.solving;

import de.tudresden.inf.st.mquat.solving.ilp.GLPKSolver;

public class GLPKHandwrittenTest extends HandwrittenTestSuite {
  @Override
  protected Solver getSolver() {
    // set to false for debugging
    return new GLPKSolver().setDeleteFilesOnExit(false);
  }
}
