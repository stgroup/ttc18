package de.tudresden.inf.st.mquat.solving;

import de.tudresden.inf.st.mquat.solving.ilp.GurobiSolver;

public class GurobiHandwrittenTest extends HandwrittenTestSuite {
  @Override
  protected Solver getSolver() {
    // set to false for debugging
    return new GurobiSolver().setDeleteFilesOnExit(false);
  }
}
