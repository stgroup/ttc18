# glpk also from https://github.com/dmccloskey/glpk/blob/master/Dockerfile

# Set the base image to Ubuntu
FROM ubuntu:latest
# Switch to root for install
USER root

RUN apt-get update -y && apt-get install -y \
	openjdk-8-jdk \
	libglpk-java \
	glpk-utils \
	--no-install-recommends \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

ENV HOME /home/user

RUN useradd --create-home --home-dir $HOME user \
    && chmod -R u+rwx $HOME \
    && chown -R user:user $HOME

# now prepare ttc18 folder
RUN mkdir /ttc18
RUN chown user:user -R /ttc18

USER user
WORKDIR /ttc18

# start optimization: first download
# see also here https://github.com/heroku/docker-gradle/blob/master/Dockerfile
ADD gradlew .
ADD gradle gradle
ADD gradle.properties .
ADD gradle gradle
ADD settings.gradle .
# following is not used as it needs too much maintenance
ADD jastadd-mquat-solver-ilp/build.gradle jastadd-mquat-solver-ilp/build.gradle
ADD jastadd-mquat-solver/build.gradle jastadd-mquat-solver/build.gradle
ADD jastadd-mquat-base/build.gradle jastadd-mquat-base/build.gradle
ADD jastadd-mquat-base/jastadd_modules jastadd-mquat-base/jastadd_modules
ADD jastadd-mquat-solver-aco/build.gradle jastadd-mquat-solver-aco/build.gradle
ADD jastadd-mquat-solver-emfer/build.gradle jastadd-mquat-solver-emfer/build.gradle
ADD jastadd-mquat-solver-simple/build.gradle jastadd-mquat-solver-simple/build.gradle
ADD jastadd-mquat-solver-random/build.gradle jastadd-mquat-solver-random/build.gradle
ADD jastadd-mquat-solver-genetic/build.gradle jastadd-mquat-solver-genetic/build.gradle
ADD jastadd-mquat-benchmark/build.gradle jastadd-mquat-benchmark/build.gradle
RUN ./gradlew --no-daemon --stacktrace build; true
RUN ./gradlew --no-daemon --stacktrace dependencies; true
# end optimization: first download

ADD . .

USER root
RUN chown user:user -R /ttc18
USER user

RUN echo "glpkPath = /usr/lib/x86_64-linux-gnu/jni" > gradle.properties
#RUN sed -i '/jastadd-mquat-solver-emfer/d' settings.gradle
#RUN sed -i '/jastadd-mquat-solver-emfer/d' jastadd-mquat-benchmark/build.gradle
#RUN sed -i '/EMFeRSolver/d' jastadd-mquat-benchmark/src/main/java/de/tudresden/inf/st/mquat/benchmark/SolverFactory.java
#RUN echo "test.enabled = false" >> ./jastadd-mquat-solver-emfer/build.gradle
RUN sed -i 's/MINUTES/SECONDS/g' jastadd-mquat-benchmark/src/main/resources/scenarios.json

# then build
RUN ./gradlew --no-daemon assemble compileTestJava
