package de.tudresden.inf.st.mquat.benchmark;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.mquat.data.TestGeneratorSettings;
import de.tudresden.inf.st.mquat.benchmark.data.BenchmarkSettings;
import de.tudresden.inf.st.mquat.benchmark.data.ScenarioData;
import de.tudresden.inf.st.mquat.benchmark.data.ScenarioSettings;
import de.tudresden.inf.st.mquat.jastadd.model.Tuple;
import de.tudresden.inf.st.mquat.solving.BenchmarkableSolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Runs all defined scenarios using every solver defined.
 *
 * @author rschoene - Initial contribution
 */
public class FullBenchmarkMain {

  private static Logger logger = LogManager.getLogger(FullBenchmarkMain.class);

  static class InvokeJavaVMBenchmark extends ScenarioBenchmark {
    private boolean useGradleConnection = false;
    private final String gradleCommand = System.getProperty("os.name")
        .toLowerCase().contains("windows") ? "./gradle.bat" : "./gradlew";
    private final String gradleTaskName = "sandboxRun";
    private final String gradleArgumentPrefix = "-PrunSettingsFile=";

    InvokeJavaVMBenchmark(BenchmarkSettings settings, int repetitions) {
      super(settings, Collections.emptyList(), repetitions);
    }

    InvokeJavaVMBenchmark setUseGradleConnection(boolean useGradleConnection) {
      this.useGradleConnection = useGradleConnection;
      return this;
    }

    @Override
    public void run() {
      // create a settings file for each solver
      List<Tuple<Path, String>> pathList = new ArrayList<>();
      for (String solverName : this.settings.solvers) {
        settings.solvers = Collections.singletonList(solverName);
        Path tempPath;
        try {
          tempPath = Files.createTempFile("runSettings", ".json");
          Utils.writeToResource(Utils.getMapper(), tempPath, settings);
        } catch (IOException e) {
          logger.catching(e);
          throw new RuntimeException("Could not create temporary file for sandBoxed use. Exiting.");
        }
        pathList.add(new Tuple<>(tempPath, solverName));
      }
      nextSolver: for (Tuple<Path, String> tuple : pathList) {
        Path path = tuple.getFirstElement();
        String solverName = tuple.getSecondElement();
        for (int i = 0; i < this.repetitions; i++) {
          if (useGradleConnection) {
            // invoke Gradle target directly
            ProjectConnection connection = GradleConnector.newConnector()
                .forProjectDirectory(new File("."))
                .connect();
            try {
              connection.newBuild()
                  .forTasks(gradleTaskName)
                  .withArguments(gradleArgumentPrefix + path.toAbsolutePath().toString())
                  .setStandardOutput(System.out)
                  .run();
            } finally {
              connection.close();
            }
          } else {
            // start a new process calling gradle
            File currentDir = getCurrentDir();
            try {
              ProcessBuilder pb = new ProcessBuilder(gradleCommand, gradleTaskName,
                  gradleArgumentPrefix + path.toAbsolutePath().toString());
              pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
              pb.redirectError(ProcessBuilder.Redirect.INHERIT);
              pb.directory(currentDir);
              Process p = pb.start();
              boolean finishedInTime = p.waitFor(settings.basic.timeoutValue * 2,
                  TimeUnit.valueOf(settings.basic.timeoutUnit));
              if(!finishedInTime) {
                logger.error("Aborting benchmark step, timeout exceeded.");
                p.destroy();
                p.destroyForcibly();
                // wait for the process to actually end
                p.waitFor();
                // write out csv entry, if the process is terminated this way
                StringBuilder sb = createRow(solverName);
                Path resultFile = getResultFilePath("");
                logger.info("Writing out to {}", resultFile);
                try (BufferedWriter writer = Files.newBufferedWriter(resultFile,
                    StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
                  writeOutResult(writer, resultFile, new AtomicInteger(1), sb);
                } catch (IOException e) {
                  logger.catching(e);
                  logger.fatal("Could not create or write to the benchmark file {}.",
                      resultFile.toAbsolutePath());
                }
                // skip remaining repetitions, as this timeout is considered as an error
                if (settings.skipOnError) {
                  continue nextSolver;
                }
              }
            } catch (IOException | InterruptedException e) {
              e.printStackTrace();
              if (settings.skipOnError) {
                continue nextSolver;
              }
            }
          }
        }
      }
    }

    StringBuilder createRow(String solverName) {
      StringBuilder sb = new StringBuilder(makeNow()).append(SEPARATOR);
      return sb.append(0).append(SEPARATOR)
          .append(settings.basic.minTopLevelComponents).append(SEPARATOR)
          .append(settings.basic.minAvgNumCompSubComponents).append(SEPARATOR)
          .append(settings.basic.minImplSubComponentDerivation).append(SEPARATOR)
          .append(settings.basic.minAvgNumCompSubComponents).append(SEPARATOR)
          .append(settings.basic.minCompSubComponentDerivation).append(SEPARATOR)
          .append(settings.basic.minComponentDepth).append(SEPARATOR)
          .append(settings.basic.minNumImplementations).append(SEPARATOR)
          .append(settings.basic.minResourceRatio).append(SEPARATOR)
          .append(settings.basic.minRequests).append(SEPARATOR)
          .append(settings.basic.minCpus).append(SEPARATOR)
          .append(settings.basic.seed).append(SEPARATOR)
          .append(-1).append(SEPARATOR)  // model.numComponents
          .append(-1).append(SEPARATOR)  // model.numImplementations
          .append(-1).append(SEPARATOR)  // modelGeneration
          .append(-1).append(SEPARATOR)  // initialObjective
          .append(solverName).append(SEPARATOR)
          .append(-1).append(SEPARATOR)  // generation time
          .append(-1).append(SEPARATOR)  // solving time
          .append(-1).append(SEPARATOR)  // objective
          .append(false).append(SEPARATOR)  // validSolution
          .append(true).append("\n");  // hadTimeout
    }

    private File getCurrentDir() {
      // hacky solution to get to the root of the repository
      File maybeCurrentDir = new File("").getAbsoluteFile();
      if ("jastadd-mquat-benchmark".equals(maybeCurrentDir.getName())) {
        maybeCurrentDir = maybeCurrentDir.getParentFile();
      }
      return maybeCurrentDir;
    }
  }

  public static void main(String[] args) {
    List<Benchmark> benchmarks = createFromConfig(args);
    if (benchmarks == null || benchmarks.isEmpty()) {
      logger.fatal("Could not create benchmarks. Exiting now.");
      return;
    }
    benchmarks.forEach(Benchmark::run);
  }

  private static List<Benchmark> createFromConfig(String[] args) {
    Logger logger = LogManager.getLogger(CustomBenchmarkMain.class);
    ObjectMapper mapper = Utils.getMapper();
    ScenarioSettings settings;
    try {
      settings = Utils.readFromResource(mapper, "scenarios.json", ScenarioSettings.class);
    } catch (IOException e) {
      logger.catching(e);
      return null;
    }
    final List<Integer> allowedIds = Arrays.stream(args)
        .map(FullBenchmarkMain::parseInt)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
    final List<String> allowedNames = Arrays.asList(args);
    final boolean takeAll = args.length == 0;
    final List<BenchmarkableSolver> solvers = settings.solvers.stream()
        .map(SolverFactory::getSolverByName).collect(Collectors.toList());
    return settings.scenarios.stream()
        .filter(data -> takeAll || allowedIds.contains(data.getId()) || allowedNames.contains(data.name))
        .map(data -> create(settings, data, solvers))
        .collect(Collectors.toList());
  }

  private static Integer parseInt(String s) {
    try {
      return Integer.parseInt(s);
    } catch (NumberFormatException e) {
      return null;
    }
  }

  private static BenchmarkSettings from(ScenarioSettings settings, ScenarioData data) {
    BenchmarkSettings result = new BenchmarkSettings();
    result.kind = "normal";
    String scenarioName = data.getId() + "_" + data.name;
    result.resultFilePattern = scenarioName + ".csv";
    result.modelFilePattern = scenarioName + ".txt";
    result.solutionFilePattern = scenarioName + "-%(solver)s.txt";
    result.logLevel = settings.logLevel;
    result.path = settings.path;
    result.solvers = settings.solvers;
    result.skipOnError = settings.skipOnError;
    TestGeneratorSettings tgs = new TestGeneratorSettings();
    tgs.minTopLevelComponents = tgs.maxTopLevelComponents = 1;
    tgs.minAvgNumImplSubComponents = tgs.maxAvgNumImplSubComponents = 0;
    tgs.minImplSubComponentDerivation = tgs.maxImplSubComponentDerivation = 0;
    tgs.minAvgNumCompSubComponents = tgs.maxAvgNumCompSubComponents = 2;
    tgs.minCompSubComponentDerivation = tgs.maxCompSubComponentDerivation = 0;
    tgs.minComponentDepth = data.depth;
    tgs.maxComponentDepth = data.depth;
    tgs.minNumImplementations = data.variants;
    tgs.maxNumImplementations = data.variants;
    tgs.minRequests = data.requests;
    tgs.maxRequests = data.requests;
    tgs.stepRequests = 1;
    tgs.minCpus = tgs.maxCpus = 1;
    tgs.minResourceRatio = data.resources;
    tgs.maxResourceRatio = data.resources;
    tgs.stepResourceRatio = 1.0;
    tgs.timeoutValue = settings.timeoutValue;
    tgs.timeoutUnit = settings.timeoutUnit;
    tgs.seed = settings.seed;
    tgs.total = settings.repetitions;
    tgs.verbose = true;
    result.updateBasic(tgs);
    return result;
  }

  private static Benchmark create(ScenarioSettings scenarioSettings, ScenarioData data,
                                  List<BenchmarkableSolver> solvers) {
    BenchmarkSettings settings = from(scenarioSettings, data);
    if (scenarioSettings.sandBoxed) {
      return new InvokeJavaVMBenchmark(settings, scenarioSettings.repetitions)
          .setUseGradleConnection(scenarioSettings.useGradleConnection);
    } else {
      return new ScenarioBenchmark(settings, solvers, scenarioSettings.repetitions);
    }
  }

}
