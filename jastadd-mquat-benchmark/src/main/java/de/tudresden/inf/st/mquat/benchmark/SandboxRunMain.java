package de.tudresden.inf.st.mquat.benchmark;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.mquat.benchmark.data.BenchmarkSettings;
import de.tudresden.inf.st.mquat.solving.BenchmarkableSolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to be executed to run a benchmark in a separate JavaVM.
 *
 * @author rschoene - Initial contribution
 */
public class SandboxRunMain {

  private static final Logger logger = LogManager.getLogger(SandboxRunMain.class);

  public static void main(String[] args) {
    // expect runSettingsFile as first argument
    String runSettingsFilename = args[0];
    // construct a ScenarioBenchmark from this setting
    // read settings first
    ObjectMapper mapper = Utils.getMapper();
    BenchmarkSettings settings;
    try {
      settings = mapper.readValue(new File(runSettingsFilename), BenchmarkSettings.class);
    } catch (IOException e) {
      logger.catching(e);
      throw new RuntimeException("Could not read settings! Exiting.", e);
    }
    // create solvers
    final List<BenchmarkableSolver> solvers = settings.solvers.stream()
        .map(SolverFactory::getSolverByName).collect(Collectors.toList());
    // repetitions will always be controlled outside of this sand-boxed environment
    ScenarioBenchmark benchmark = new ScenarioBenchmark(settings, solvers, 1);
    benchmark.run();
  }

}
